---


# LIS 4381 - Mobile Web Applications Development

## James White / jdw19g

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
        * Screenshot of AMPPS running 
    * Install Java Developer Kit
        * Screenshot of JDK java "Hello"
    * Install Visual Studio Code
    * Install Android Studio
        * Screenshot of Android Studio "My First App" 
    * provide git command descriptions
    * Create Bitbucket Repository
    * Complete Bitbucket tutorial (bitbucketstationlocations)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Develop an application that shows a recipe
        * Screenshots of application running
    * Skill set 1: Even or Odd application
        * Screenshots of skill set 1 running
    * Skill set 2: Largest of Two Integers application
        * Screenshots of skill set 2 running
    * Skill set 3: Arrays and Loops Application
        * Screenshot of skill set 3 running

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Develop an application that calculates concert ticket prices
        * Screenshots of application running
    * Develop a pestore database
        * Screenshots of ERD and tables
    * Skill set 4: Decision Structures application
        * Screenshots of skill set 4 running
    * Skill set 5: Nested Structures application
        * Screenshots of skill set 5 running
    * Skill set 6: Methods Application
        * Screenshot of skill set 6 running
    * Important links:
        * Bitbucket Repository
        * Petstore database ERD and SQL
        * Skill set 4
        * Skill set 5
        * Skill set 6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Develop a web application that:
        * Displays the current work done in LIS 4381 with working navigation and clickable links to each assignment and/or project.
        * Has a working rotating carousel with at least three images, clickable links, and hover capabilities.
        * Has a web form with client-side validations using regex expressions.
	* Skill set 10: ArrayList Introduction Program
        * Screenshots of skill set 10 running
    * Skill set 11: Alpha, Numeric, or Special Character Detector
        * Screenshots of skill set 11 running
    * Skill set 12: Temperature Conversion Program
        * Screenshot of skill set 12 running
    * Important links:
        * Bitbucket Repository
        * Skill set 10
        * Skill set 11
        * Skill set 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Develop an additional web form that:
        * Displays all pet stores listed in the petstore table.
        * Has a "working add pet store" button which allows for the addition of pet stores *only* if valid data is entered using server side validation.
        * Has non-functional "edit" and "delete" buttons in preperation for P2.
	* Skill set 13: Sphere Volume Calculator Program
        * Screenshot of skill set 13 running
    * Skill set 14: Simple Calculator Web Application
        * Screenshots of skill set 14 running
    * Skill set 15: Write/Read File Web Application
        * Screenshot of skill set 15 running
    * Important links:
        * Bitbucket Repository
		* Localhost Link
        * Skill set 13
        * Skill set 14
        * Skill set 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    * Develop an application that can be used as a business card (includes contact info and interests)
        * a) Shows a launcher icon image and displays it both activities(screens).
        * b) Adds background color(s) to both activities.
        * c) Adds border around image and button.
        * d) Adds text shadow(button).
        * e) Changes the text shadow.
     * Skill set 7: Random Array Data Validation application
        * Screenshots of skill set 7 running
    * Skill set 8: Largest of Three Numbers application
        * Screenshots of skill set 8 running
    * Skill set 9: Array Runtime Data Validation Application
        * Screenshot of skill set 9 running
    * Important links:
        * Bitbucket Repository
        * Skill set 7
        * Skill set 8
        * Skill set 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Modify assignmenet 5 to:
        * a) Successfully edit a record in the petstore table.
        * b) validate if edited data is valid using server-side validation.
        * c) successfully delete a record from the petstore table.
    * Create and view a __working__ RSS feed.

