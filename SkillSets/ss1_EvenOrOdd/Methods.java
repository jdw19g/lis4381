import java.util.Scanner;

public class Methods
{
    //Create method without returning any value (without method)
    public static void GetRequirements()
    {
        System.out.println("");
        System.out.println("Developer: James White");
        System.out.println("Program evaluates whether or not a number is even or odd.");
        System.out.println("Note: Program does *NOT* check for non-numeric characters.");
        System.out.println(""); //Prints blank line
    }

    public static void evaluateNumber()
    {
        int x = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if (x % 2 == 0)
        {
            System.out.println(x + " is an even number.");
        }
        else
        {
            System.out.println(x + " is an odd number.");
        }
    }
}
