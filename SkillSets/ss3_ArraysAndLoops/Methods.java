import java.util.Scanner;

public class Methods
{
    //Create method without returning any value (without method)
    public static void GetRequirements()
    {
        System.out.println();
        System.out.println("Developer: James White");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do...while");
        System.out.println(); //Prints blank line
    }

    public static void arrayLoops() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Use the following format for string: dog, cat, bird, fish, insect");
        System.out.println("The string will then be split apart and placed into an array.");
        System.out.println("The array will then be sent back as a list.");
        System.out.println("Enter string of values: ");
        String input = sc.nextLine();
        String[] animals = input.split(", ");
        System.out.println();

        
        System.out.println("for loop: ");
        for(int i = 0; i < animals.length; i++)
        {
            System.out.println(animals[i]);
        }

        System.out.println("\nenhanced for loop: ");
        for(String test : animals)
        {
            System.out.println(test);
        }

        System.out.println("\nwhile loop: ");
        int i=0;
        while (i < animals.length)
        {
            System.out.println(animals[i]);
            i++;
        }

        i=0;
        System.out.println("\ndo...while loop: ");
        do{
            System.out.println(animals[i]);
            i++;
        }
        while (i < animals.length);
    }
}