class Main
{
    public static void main(String args[]) 
    {
        Methods.getRequirements();

        int[] userArray = Methods.createArray(); //Java style array

        Methods.generatePseudoRandomNumbers(userArray); //Pass array
    }
}