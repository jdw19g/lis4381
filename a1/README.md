---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Questions
4. Bitbucket Repository Links:
    
    a) https://bitbucket.org/jdw19g/lis4381
    
    b) https://bitbucket.org/jdw19g/bitbucketstationlocations

#### README.md file should include the following items:

* Screenshot of AMPPS application running

* Screenshot of running JDK "java Hello"

* Screenshot of running Android Studio "My First App"

* git commands w/short descriptions
    1. git init: Create an empty Git repository or reinitialize an existing one
    2. git status: Show the working tree status
    3. git add: Add file contents to the index
    4. git commit: Record changes to the repository
    5. git push: Update remote refs along with associated objects
    6. git pull: Fetch from and integrate with another repository or a local branch
    7. git help: Display help information about Git

#### Assignment Screenshots:
| | |
|---|---|
| *Screenshot of AMPPS running*: ![1](img/a1_ampps_PhpRunning.png) | *Screenshot of JDK java "Hello" running*: ![2](img/a1_JDK_Hello.png) |
| *Screenshot of Android Studio's "My First App" running*: ![3](img/a1_AdroidStudio_MyFirstApp.png) | |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jdw19g/bitbucketstationlocations/ "Bitbucket Station Locations")