<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 1 requirements and images.">
		<meta name="author" content="James White">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style type="text/css">
		 h1
		 {
			 margin: 0;     
			 color: #a10330;
			 padding-top: 50px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		</style>		
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Created a Bitbucket repository to have distributed version control, downloaded Ampps and ran a basic apache page in a browser, and downloaded and created an android studio beginner app. 
				</p>

				<h4>AMPPS Installation</h4>
				<img src="img/a1_ampps_PhpRunning.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>JDK Java "Hello" running</h4>
				<img src="img/a1_JDK_Hello.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio's "My First App" running</h4>
				<img src="img/a1_AdroidStudio_MyFirstApp.png" class="img-responsive center-block" alt="Android Studio Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
