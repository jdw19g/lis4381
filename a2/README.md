
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Assignment 2 Requirements:

*Six parts:*

1. Develop an application that:
    * a) Includes instructions for a recipe.
    * b) Includes a photo of the recipe.
    * c) Create a button.
    * d) Change the background color.
    * e) Change the text color.
2. Skill set 1: Even or Odd Calculator
    * Skill set 1 link: [Skill Set 1](../SkillSets/ss1_EvenOrOdd "Skill set 1 Main.java and Methods.java")
3. Skill set 2: Largest of Two Integers Calculator
    * Skill set 2 link: [Skill Set 2](../SkillSets/ss2_LargestOfTwoIntegers "Skill set 2 Main.java and Methods.java")
4. Skill set 3: Arrays and Loops
    * Skill set 3 link: [Skill Set 3](../SkillSets/ss3_ArraysAndLoops "Skill set 3 Main.java and Methods.java")
5. Questions
6. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381

#### README.md file should include the following items:

* Screenshot the recipe application running

* Screenshot of Even or Odd Calculator application running

* Screenshot of Largest of Two Integers Calculator application running

* Screenshot of Arrays and Loops application running

---

#### Assignment Screenshots:
| | |
|---|---|
| *Screenshot of Recipe Application's main page*: ![1](img/a2_RecipeApp1.png) | *Screenshot of Recipe application's instructions page*: ![2](img/a2_RecipeApp2.png) |
| *Screenshot of Even or Odd application running pt.1*: ![3](img/ss1_Even.png) | *Screenshot of Even or Odd application running pt.2*: ![4](img/ss1_Odd.png) |
| *Screenshot of Largest of Two Integers application running pt.1*: ![5](img/ss2_LargerNum.png) | *Screenshot of Largest of Two Integers application running pt.2*: ![6](img/ss2_EqualNum.png) |
| *Screenshot of Arrays and Loops application running*: ![7](img/ss3_ArraysAndLoops.png) | |