<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 2 requirements and images.">
		<meta name="author" content="James White">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style type="text/css">
		 h1
		 {
			 margin: 0;     
			 color: #a10330;
			 padding-top: 50px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		</style>		
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Created a mobile recipe app in android studio. This project included changing the default background color and text. To add extra flair I added a checklist for the ingredients. I also have pictures of skill sets one, two, and three.
				</p>

				<h4><strong>Bruschetta Home Page</strong></h4>
				<img src="img/a2_RecipeApp1.png" class="img-responsive center-block" alt="Image of bruschetta home page">

				<h4><strong>Bruschetta Recipe Page</strong></h4>
				<img src="img/a2_RecipeApp2.png" class="img-responsive center-block" alt="Image of bruschetta recipe page">

				<h4><strong>Skill set 1:</strong> Even or Odd Calculator</h4>
				<img src="img/ss1_Even.png" class="img-responsive center-block" alt="image of even or odd calculator running part 1">
				
				<h4><strong>Skill set 1:</strong> Even or Odd Calculator</h4>
				<img src="img/ss1_Odd.png" class="img-responsive center-block" alt="image of even or odd calculator running part 2">
				
				<h4><strong>Skill Set 2:</strong> Larger of Two Numbers Calculator</h4>
				<img src="img/ss2_EqualNum.png" class="img-responsive center-block" alt="image of larger of two numbers app running part 1">
				
				<h4><strong>Skill Set 2:</strong> Larger of Two Numbers Calculator</h4>
				<img src="img/ss2_LargerNum.png" class="img-responsive center-block" alt="image of larger of two numbers app running part 2">
				
				<h4><strong>Skill Set 3:</strong> Arrays and Loops</h4>
				<img src="img/ss3_ArraysAndLoops.png" class="img-responsive center-block" alt="Image of arrays and loops skillset running">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
