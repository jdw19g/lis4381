
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Assignment 3 Requirements:

*Six parts:*

1. Develop an application that:
    * a) Shows a launcher icon image and displays it in the activity(screen).
    * b) Adds color(s) to activity controls.
    * c) Adds border around image and button.
    * d) Adds text shadow(button).
    * e) Changes the text color.
2. Develop a database that:
    * a) Has three tables.
    * b) Shows relationships along with primary and foreign keys.
    * c) Has a minimum of ten records in each table.
3. Skill set 4: Decision Structures
    * Skill set 4 link: [Skill Set 4](../SkillSets/ss4_DecisionStructures "Skill set 4 Main.java and Methods.java")
4. Skill set 5: Nested Structures
    * Skill set 5 link: [Skill Set 5](../SkillSets/ss5_NestedStructures "Skill set 5 Main.java and Methods.java")
5. Skill set 6: Methods
    * Skill set 6 link: [Skill Set 6](../SkillSets/ss6_Methods "Skill set 6 Main.java and Methods.java")
6. Questions
. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381

#### README.md file should include the following items:

* Screenshot Concert Ticket Calculator application running

* Screenshot of ERD along with selects from each table

* Screenshot of Decision Structure application running

* Screenshot of Nested Structures application running

* Screenshot of Methods application running

---

#### Assignment Screenshots:
| | |
|---|---|
| *__Screenshot of Concert Ticket Calculator (before calculations)__*: ![1](img/app_pt1.png) | *__Screenshot of Concert Ticket Calculator (after calculations)__*: ![2](img/app_pt2.png) |

*__Screenshot of ERD__*: ![3](img/erd.png)

*__Screenshot of petstore table__*:

![4](img/pst.png)

*__Screenshot of pet table__*:

![5](img/pet.png)

*__Screenshot of customer table__*:

![6](img/cus.png)

|||
|---|---|
| *__Screenshot of Decision Structure application running pt.1__*: ![7](img/ss4_pt1.png) | *__Screenshot of Decision Structure application running pt.2__*: ![8](img/ss4_pt2.png) |
| *__Screenshot of Nested Structures application running__*: ![9](img/ss5_pt1.png) | *__Screenshot of Methods application running__*: ![10](img/ss6_pt1.png) |

---

#### *Important Links:*
* *Bitbucket Repository Link:* https://bitbucket.org/jdw19g/lis4381
* Petstore Database link: [Petstore Database](../SkillSets/a3_erd "a3.mwb and a3.sql")
* Skill set 4 link: [Skill Set 4](../SkillSets/ss4_DecisionStructures "Skill set 4 Main.java and Methods.java")
* Skill set 5 link: [Skill Set 5](../SkillSets/ss5_NestedStructures "Skill set 5 Main.java and Methods.java")
* Skill set 6 link: [Skill Set 6](../SkillSets/ss6_Methods "Skill set 6 Main.java and Methods.java")


