<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 3 requirements and images.">
		<meta name="author" content="James White">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style type="text/css">
		 h1
		 {
			 margin: 0;     
			 color: #a10330;
			 padding-top: 50px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		</style>		
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Design and develop a mobile app that can calculate a cost for a number of tickets for certain concerts. The app must include a launcher icon, a button and a selecter, different colored background, and a border around the image. I also had to create a small pet store database using MySQL Workbench where I created an ERD of three tables and filled them with ten records each. Lastly, I worked on skill set four, five, and six.
				</p>

				<h4><strong>Concert Application Before Calculation</strong></h4>
				<img src="img/app_pt1.png" class="img-responsive center-block" alt="Image of Concert Application Before Calculation">

				<h4><strong>Concert Application After Calculation</strong></h4>
				<img src="img/app_pt2.png" class="img-responsive center-block" alt="Image of Concert Application After Calculation">

				<h4><strong>ERD</strong></h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="image of ERD showing all three tables and relationships">
				
				<h4><strong>Customer Table</strong></h4>
				<img src="img/cus.png" class="img-responsive center-block" alt="image of Customer Table in MySQL Workbench">
				
				<h4><strong>Pet Table</strong></h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="image of Pet Table in MySQL Workbench">
				
				<h4><strong>Petstore Table</strong></h4>
				<img src="img/pst.png" class="img-responsive center-block" alt="image of Petstore Table in MySQL Workbench">
				
				<h4><strong>Skill set 4:</strong> Decision Structures</h4>
				<img src="img/ss4_pt1.png" class="img-responsive center-block" alt="image of Decision Structures app running part 1">
				
				<h4><strong>Skill Set 4:</strong> Decision Structures</h4>
				<img src="img/ss4_pt2.png" class="img-responsive center-block" alt="image of Nested Structures app running part 2">
				
				<h4><strong>Skill Set 5:</strong> Nested Structures</h4>
				<img src="img/ss5_pt1.png" class="img-responsive center-block" alt="image of Nested Structures app running">
				
				<h4><strong>Skill Set 6:</strong> Methods</h4>
				<img src="img/ss6_pt1.png" class="img-responsive center-block" alt="Image of methods app running">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
