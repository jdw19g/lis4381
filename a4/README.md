
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Assignment 4 Requirements:

*5 parts:*

1. Develop a web application that:
    * a) Displays the current work done in LIS 4381 with working navigation and clickable links to each assignment and/or project.
    * b) Has a working rotating carousel with at least three images, clickable links, and hover capabilities.
    * c) Has a web form with client-side validations using regex expressions.
2. Skill set 10: ArrayList Introduction Program
    * Skill set 10 link: [Skill Set 10](../SkillSets/ss10_ArrayList "Skill set 10 Main.java and Methods.java")
3. Skill set 11: Alpha, Numeric, or Special Character Detector
    * Skill set 11 link: [Skill Set 11](../SkillSets/ss11_AlphaNumericSpecial "Skill set 11 Main.java and Methods.java")
4. Skill set 12: Temperature Conversion Program
    * Skill set 12 link: [Skill Set 12](../SkillSets/ss12_TemperatureConversion "Skill set 12 Main.java and Methods.java")
5. Questions
. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381

#### README.md file should include the following items:

* Screenshot LIS 4381 mobile web application running

* Screenshot of petstore form validation

* Screenshot of ArrayList Introduction Program running

* Screenshot of Alpha, Numeric, or Special Character Detector running

* Screenshot of Temperature Conversion Program running

---

#### Assignment Screenshots:
| | |
|---|---|
| *__Screenshot of Home Page__*: ![1](img/home.png) | *__Screenshot A1 Page__*: ![2](img/a1.png) |
| *__Screenshot of A2 Page__*: ![3](img/a2.png) | *__Screenshot A3 Page__*: ![4](img/a3.png) |
| *__Screenshot of p1 Page__*: ![5](img/p1.png) | |
| *__Screenshot of A4 Page Failed Validation__*: ![6](img/a4_failed.png) | *__Screenshot A4 Page Passed Validation__*: ![7](img/a4_passed.png) |
| *__Screenshot of Skill Set 10: ArrayList Introduction Program__*: ![8](img/ss10.png) | *__Screenshot of Skill Set 11: Alpha, Numeric, or Special Character Detector__*: Part 1 ![9](img/ss11_pt1.png) |
| *__Screenshot of Skill Set 11: Alpha, Numeric, or Special Character Detector__*: part 2![10](img/ss11_pt2.png) | *__Screenshot of Skill Set 12: Temperature Conversion Program__*:![10](img/ss12.png) |

---

#### *Important Links:*
* *Bitbucket Repository Link:* https://bitbucket.org/jdw19g/lis4381
* Skill set 10 link: [Skill Set 10](../SkillSets/ss10_ArrayList "Skill set 10 Main.java and Methods.java")
* Skill set 11 link: [Skill Set 11](../SkillSets/ss11_AlphaNumericSpecial "Skill set 11 Main.java and Methods.java")
* Skill set 12 link: [Skill Set 12](../SkillSets/ss12_TemperatureConversion "Skill set 12 Main.java and Methods.java")


