
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Assignment 5 Requirements:

*5 parts:*

1. Develop an additional web form that:
    * a) Displays all pet stores listed in the petstore table.
    * b) Has a "working add pet store" button which allows for the addition of pet stores *only* if valid data is entered using server side validation.
    * c) Has non-functional "edit" and "delete" buttons in preperation for P2.
2. Skill set 13: Sphere Volume Calculator Program
    * Skill set 13 link: [Skill Set 13](../SkillSets/ss13_SphereVolumeCalculator "Skill set 13 Main.java and Methods.java")
3. Skill set 14: Simple Calculator Web Application
    * Skill set 14 link: [Skill Set 14](../SkillSets/ss14_SimpleCalculator "Skill set 14 web code")
4. Skill set 15: Write/Read File Web Application
    * Skill set 15 link: [Skill Set 15](../SkillSets/ss15_WriteReadFile "Skill set 15 web code")
5. Questions
6. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381
7. Localhost link:
	* a) http://localhost:88/lis4381/index.php

#### README.md file should include the following items:

* Screenshot LIS 4381 web application running

* Screenshot of server side form validation

* Screenshot of Sphere Volume Calculator Program running

* Screenshot of Simple Calculator Web Application running

* Screenshot of Write/Read File Web Application running

---

#### Assignment Screenshots:
*__Screenshot of Invalid Pet Store Name__*: ![1](img/a5_AddPetstoreInvalid.png)

---

*__Screenshot of Invalid Pet Store Name Error__*: ![2](img/a5_ProcessFailed.png)

---

*__Screenshot of Valid Pet Store Name__*: ![3](img/a5_AddPetstoreValid.png)

---

*__Screenshot of Valid Pet Store Added to Database__*: ![4](img/a5_Index.png)

---

*__Screenshot of Skill Set 13: Sphere Volume Calculator__*: ![5](img/ss13_pt1.png)

---

*__Screenshot of Skill Set 14: Simple Calculator Using Addition__*: ![6](img/ss14_AdditionIndex.png)

---

*__Screenshot of Skill Set 14: Simple Calculator Showing Addition Process__*: ![7](img/ss14_AdditionProcess.png)

---

*__Screenshot of Skill Set 14: Simple Calculator Using Division__*: ![8](img/ss14_DivisonIndex.png)

---

*__Screenshot of Skill Set 14: Simple Calculator Showing Divsion Validation__*: ![9](img/ss14_DivisionProcess.png)

---

*__Screenshot of Skill Set 15: Write/Read File Index.php Page__*: ![10](img/ss15_Index.png)

---

*__Screenshot of Skill Set 15: Write/Read File Process.php Page__*: Part 1 ![11](img/ss15_Process.png)

---

#### *Important Links:*
* *Bitbucket Repository Link:* https://bitbucket.org/jdw19g/lis4381
* *Localhost Link:* http://localhost:88/lis4381/index.php
* Skill set 13 link: [Skill Set 13](../SkillSets/ss13_SphereVolumeCalculator "Skill set 13 Main.java and Methods.java")
* Skill set 14 link: [Skill Set 14](../SkillSets/ss14_SimpleCalculator "Skill set 14 web code")
* Skill set 15 link: [Skill Set 15](../SkillSets/ss15_WriteReadFile "Skill set 15 web code")


