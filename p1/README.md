
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Project 1 Requirements:

*Five parts:*

1. Develop an application that:
    * a) Shows a launcher icon image and displays it both activities(screens).
    * b) Adds background color(s) to both activities.
    * c) Adds border around image and button.
    * d) Adds text shadow(button).
    * e) Changes the text shadow.
2. Skill set 7: Random Array Data Validation
    * Skill set 7 link: [Skill Set 7](../SkillSets/ss7_RandomArrayDataValidation "Skill set 7 Main.java and Methods.java")
3. Skill set 8: Largest of Three Numbers
    * Skill set 8 link: [Skill Set 8](../SkillSets/ss8_LargestThreeNumbers "Skill set 8 Main.java and Methods.java")
4. Skill set 9: Array Runtime Data Validation
    * Skill set 9 link: [Skill Set 9](../SkillSets/ss9_ArrayRuntimeDataValidation "Skill set 9 Main.java and Methods.java")
5. Questions
. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381

#### README.md file should include the following items:

* Screenshot of business card home page running

* Screenshot of business card details page running

* Screenshot of Random Array Data Validation application running

* Screenshot of Largest of Three Numbers application running

* Screenshot of Array Runtime Data Validation application running

---

#### Assignment Screenshots:
| | |
|---|---|
| *__Screenshot of Business Card Home Page__*: ![1](img/p1_pt1.png) | *__Screenshot of Business Card Details Page__*: ![2](img/p1_pt2.png) |
| *__Screenshot of Random Array Data Validation application running__*: ![3](img/ss7.png) | *__Screenshot of Largest of Three Numbers application running__*: ![4](img/ss8.png) |
| *__Screenshot of Array Runtime Data Validation application running__*: ![5](img/ss9.png) | |

---

#### *Important Links:*
* *Bitbucket Repository Link:* https://bitbucket.org/jdw19g/lis4381
* Skill set 7 link: [Skill Set 7](../SkillSets/ss7_RandomArrayDataValidation "Skill set 7 Main.java and Methods.java")
* Skill set 8 link: [Skill Set 8](../SkillSets/ss8_LargestThreeNumbers "Skill set 8 Main.java and Methods.java")
* Skill set 9 link: [Skill Set 9](../SkillSets/ss9_ArrayRuntimeDataValidation "Skill set 9 Main.java and Methods.java")


