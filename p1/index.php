<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1 requirements and images.">
		<meta name="author" content="James White">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style type="text/css">
		 h1
		 {
			 margin: 0;     
			 color: #a10330;
			 padding-top: 50px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		</style>		
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Designed and developed a mobile application to act as a mobile business card that anyone can download and look at. The home page was to include an image of myself along with a button. The second page included details such as contact information and interests. Also, included are skill sets seven, eight, and nine.
				</p>

				<h4><strong>Mobile Business Card Home Page</strong></h4>
				<img src="img/p1_pt1.png" class="img-responsive center-block" alt="Image of Mobile Business Card Home Page">

				<h4><strong>Mobile Business Card Details Page</strong></h4>
				<img src="img/p1_pt2.png" class="img-responsive center-block" alt="Image of Mobile Business Card Details Page">
				
				<h4><strong>Skill set 7:</strong> Random Array Data Validation</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="image of Random Array Data Validation app running">
				
				<h4><strong>Skill Set 8:</strong> Largest of Three Numbers</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="image of Largest of Three Numbers app running">
				
				<h4><strong>Skill Set 9:</strong> Array Runtime Data Validation</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="image of Array Runtime Data Validation app running">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
