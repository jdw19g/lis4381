
---


# LIS 4381 - Mobile Web Applications Development

## jdw19g / James White

### Project 2 Requirements:

*5 parts:*

1. Modify assignmenet 5 to:
    * a) Successfully edit a record in the petstore table.
    * b) validate if edited data is valid using server-side validation.
    * c) successfully delete a record from the petstore table.
2. Create and view a __working__ RSS feed.
3. Questions
4. Bitbucket Repository Link:
    * a) https://bitbucket.org/jdw19g/lis4381
5. Localhost link:
	* a) http://localhost:88/lis4381/index.php

#### README.md file should include the following items:

* Screenshot LIS 4381 web application running

* Screenshot of edit form functionality

* Screenshot of server side edit form validation

* Screenshot of delete functionality

---

#### Assignment Screenshots:
*__Screenshot of LIS4381 Homepage__*: ![1](img/HomePage.png)

---

*__Screenshot of Project 2 index.php__*: ![2](img/Index.png)

---

*__Screenshot of Edit Page__*: ![3](img/Edit.png)

---

*__Screenshot of Failed Edit Validation__*: ![4](img/EditFailed.png)

---

*__Screenshot of Passed Edit Validation__*: ![5](img/EditPassed.png)

---

*__Screenshot of Delete Functionality__*: ![6](img/Delete.png)

---

*__Screenshot of Deleted Record__*: ![7](img/DeleteSuccess.png)

---

*__Screenshot of RSS Feed__*: ![8](img/RSS.png)

---

#### *Important Links:*
* *Bitbucket Repository Link:* https://bitbucket.org/jdw19g/lis4381
* *Localhost Link:* http://localhost:88/lis4381/index.php
